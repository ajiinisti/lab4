from django.shortcuts import render

def index(request):
    return render(request, "index.html")

def pengalaman(request):
    return render(request, "pengalaman.html")

def keterampilan(request):
    return render(request, "keterampilan.html")

def tentangsaya(request):
    return render(request, "tentangsaya.html")
