from django.urls import path
from pages.views import index,keterampilan,tentangsaya,pengalaman
#url for app

urlpatterns = [
    # re_path(r'^$', index, name='index'),
    path('', index,name = 'index'),
    path('keterampilan/', keterampilan, name='keterampilan'),
    path('tentangsaya/', tentangsaya , name='tentangsaya'),
    path('pengalaman/', pengalaman, name='pengalaman'),
]
